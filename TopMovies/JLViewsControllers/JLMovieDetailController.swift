//
//  JLMovieDetailController.swift
//  TopMovies
//
//  Created by Juan Carlos Lugardo on 06/05/21.
//

import UIKit
import SafariServices

class JLMovieDetailController: UIViewController {

    @IBOutlet weak var imgBackCover: UIImageView!
    @IBOutlet weak var imgCover: UIImageView!
    
    @IBOutlet weak var stckRate: UIStackView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAverage: UILabel!
    @IBOutlet weak var lblGenere: UILabel!
    @IBOutlet weak var lblDirector: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblRigths: UILabel!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var coverTopConstraint: NSLayoutConstraint!
    
    var presenter   : JLMoviesPresenter = JLMoviesPresenter()
    var movie: Movie?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setClearBG()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func updateUI(){
        if let mv = movie{
            lblTitle.text = mv.title
            lblAverage.text = "\(mv.vote_average)"
            lblGenere.text = mv.getGnresString()
            lblDirector.text = mv.homepage
            lblReleaseDate.text = mv.release_date
            lblDescription.text = mv.overview
            lblRigths.text = mv.getCompaniesString()
            if let imgCoverD = URL(string: IMG_BUCKET + mv.poster_path){
                imgCover.downloaded(from: imgCoverD)
                imgBackCover.downloaded(from: imgCoverD)
            }
            
            addHandlerToLabel(lblDirector)
            
            paintStars()
        }
        
        topConstraint.constant = self.view.frame.size.height * 0.15
        coverTopConstraint.constant = self.view.frame.size.height * 0.11
        view.layoutIfNeeded()
    }
    
    func addHandlerToLabel(_ lbl : UILabel){
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector((self.openHomePage(_:)))))
    }
    
    func getData(){
        if let mv = movie{
            presenter.delegate = self
            presenter.getDetailMovie(mv.id,true)
        }
        
    }
    
    @objc func openHomePage(_ gesture : UITapGestureRecognizer){
        if gesture.state == .ended{
            guard let lbl = gesture.view as? UILabel else {return}
            if let url = URL(string: lbl.text ?? "") {
                let svc = SFSafariViewController(url: url)
                present(svc, animated: true, completion: nil)
            }
        }
    }
    
    func paintStars(){
        guard let mv = self.movie else {return }
        let modStar = Int(mv.vote_average / 2)

        for i in 0..<5{
            if let imgS = self.stckRate.subviews[i] as? UIImageView{
                if modStar > i{
                    imgS.image = UIImage(named: "ic_star_fill")
                }else{
                    imgS.image = UIImage(named: "ic_star_stroke")
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension JLMovieDetailController : JLMoviesPresenterDelegate{
    func getDetaoMovie(_ movie: Movie) {
        self.movie = movie
        DispatchQueue.main.async {
            self.updateUI()
        }
    }
    
    func getTopMovies(_ info: MoviesResponse) {}
    
    
}

//
//  ViewController.swift
//  TopMovies
//
//  Created by Juan Carlos Lugardo on 05/05/21.
//

import UIKit

class JLViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var presenter   : JLMoviesPresenter = JLMoviesPresenter()
    var movies      : [Movie] = []
    var selectedMovie : Movie?
    var currentPage : Int = 1
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configTableview()
        getData(false)
    }

    func getData(_ refresh: Bool){
        presenter.delegate = self
        presenter.getInfo(.getTop, currentPage,refresh)
        
    }

    func configTableview(){
        tableView.delegate      = self
        tableView.dataSource    = self
        tableView.registerNibCell("JLCoverCell", "coverCell")
        tableView.addSubview(refreshControl)
        
        refreshControl.addTarget(self, action: #selector(self.updateRefresh), for: .valueChanged)
        
        tableView.tableFooterView = configureFooterView()
    }
    
    @objc func updateRefresh(){
        refreshControl.beginRefreshing()
        self.getData(true)
    }
    
    
    func configureFooterView()-> UIView{
        let btnNext = UIButton(type: .custom)
        btnNext.setTitle("Siguiente", for: .normal)
        btnNext.addTarget(self, action: #selector(self.getNextPage), for: .touchUpInside)
        
        let btnBack = UIButton(type: .custom)
        btnBack.setTitle("Anterior", for: .normal)
        btnBack.addTarget(self, action: #selector(self.getBackPage), for: .touchUpInside)
        
        btnBack.isHidden = currentPage == 1
        
        let stkButtons = UIStackView(arrangedSubviews: [btnBack,btnNext])
        stkButtons.alignment = .center
        stkButtons.distribution = .fillEqually
        
        stkButtons.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 60)
        return stkButtons
    }
    
    @objc func getNextPage(){
        currentPage = currentPage + 1
        self.getData(true)
    }
    @objc func getBackPage(){
        currentPage = currentPage - 1
        self.getData(true)
    }
    
    func updateTableStatus(){
        tableView.tableFooterView = configureFooterView()
        if tableView.numberOfSections > 0{
            if tableView.numberOfRows(inSection: 0) > 0{
                tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
        }
        tableView.reloadData()
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let dvc = segue.destination as? JLMovieDetailController{
            dvc.movie = selectedMovie
        }
    }
    
}


extension JLViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    

}

extension JLViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "coverCell", for: indexPath) as? JLCoverCell{
            cell.setMovie = movies[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMovie = movies[indexPath.row]
        self.performSegue(withIdentifier: "sw_detail", sender: self)
    }
    
    
}


extension JLViewController : JLMoviesPresenterDelegate{
    func getDetaoMovie(_ movie: Movie) {    }
    
    func getTopMovies(_ info: MoviesResponse) {
        movies = info.results
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
            self.updateTableStatus()
        }
    }
        
}

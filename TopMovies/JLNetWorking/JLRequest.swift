//
//  JLRequest.swift
//  TopMovies
//
//  Created by Juan Carlos Lugardo on 05/05/21.
//

import Foundation


enum Movies{
    typealias succesCodable<T: Codable> = (T?)
    typealias normalError = (Swift.Error) -> Void
    
    case topMovies(_ page : Int)
    case detail(_ idMovie: Int)
    
    var httpMethod: String {
        switch self {
        case .topMovies:
            return "get"
        default:
            return "get"
        }
    }
    
    var request: URLRequest {
        get {
            var request = URLRequest(url: self.url())
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = self.httpMethod
            return request
        }
    }    
    
    func endpoint() -> String {
        switch self {
        case .topMovies:
            return "\(SERVER_URL)/top_rated"
        case .detail(let idMovie):
            return "\(SERVER_URL)/\(idMovie)"
        }
    }
    
    
    func url() -> URL {
        var components = URLComponents(string: self.endpoint())!
        
        var queryItems: [URLQueryItem] = []
        queryItems.append(URLQueryItem(name: "language", value: "es_MX"))
        queryItems.append(URLQueryItem(name: "api_key", value: API_KEY))
        
        switch self {
        case .topMovies(let page):
            queryItems.append(URLQueryItem(name: "page", value: "\(page)"))
        case .detail: break
        }
        components.queryItems = queryItems

        return components.url!

    }
    
    func makeTaks<T: Codable>(_ type : T.Type,success successCallback: @escaping (succesCodable<T>) -> Void, error errorCallback: @escaping normalError) {
        
        let task = URLSession.shared.dataTask(with: request){ data, response, error in
            if let error = error{
                print(error.localizedDescription)
                errorCallback(error)
            }else{
                if let data = data{
                    successCallback((data.getStruct(T.self)))
                }else{
                    let error = NSError(domain:"https://api.themoviedb.org/", code:0, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
                    errorCallback(error)
                }
            }
        }
        task.resume()
    }

}




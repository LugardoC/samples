//
//  JLMoviesPresenter.swift
//  TopMovies
//
//  Created by Juan Carlos Lugardo on 06/05/21.
//

import Foundation

protocol JLMoviesPresenterDelegate {
    func getTopMovies(_ info: MoviesResponse)
    func getDetaoMovie(_ movie: Movie)
}

enum typeRequest : Int{
    case getTop = 0
    case getDetail = 1
}
class JLMoviesPresenter {
    
    var delegate: JLMoviesPresenterDelegate?
    var mov : Movies!
    var currentPage : Int = 0
    var topInfo = MoviesResponse()
    var DBM = JLDataBaseManager.shared
    
    init(){}
    
    func getInfo(_ type: typeRequest,_ param: Int,_ refresh : Bool? = false) {
        switch type {
        case .getTop:
            currentPage = param
            self.getMovies(param,refresh)
        case .getDetail:
            self.getDetailMovie(param,refresh ?? false)
        }
    }
    
    func getMovies(_ page: Int,_ refresh: Bool? = false) {
        guard let dl = self.delegate else {return}
        if let localMovies = DBM.getLocalObjects(refresh ?? false),localMovies.results.count > 0 && !(refresh ?? false){
            dl.getTopMovies(localMovies)
        }else{
            mov = Movies.topMovies(page)
            mov.makeTaks(MoviesResponse.self, success: {data in
                if let _ = data{
                    dl.getTopMovies(data!)
                    self.DBM.saveMovies(data!)
                }else{
                    dl.getTopMovies(MoviesResponse())
                }
            }, error: { error in
                print(error.localizedDescription)
            })
        }
        
    }
    
    func getDetailMovie(_ idMovie: Int,_ refresh: Bool){
        guard let dl = self.delegate else {return}
        if let localM = DBM.getLocalMovie(idMovie, refresh), localM.genres.count > 0{
            dl.getDetaoMovie(localM)
        }else{
            mov = Movies.detail(idMovie)
            mov.makeTaks(Movie.self, success: { data in
                if let m = data{
                    dl.getDetaoMovie(m)
                }
            }, error: { error in
                print(error.localizedDescription)
            })
        }

    }
    
}

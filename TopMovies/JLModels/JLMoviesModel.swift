//
//  JLMoviesModel.swift
//  TopMovies
//
//  Created by Juan Carlos Lugardo on 06/05/21.
//

import Foundation
import RealmSwift

struct MoviesResponse   : Codable{
    var page            : Int       = 0
    var total_pages     : Int       = 0
    var total_results   : Int       = 0
    var results         : [Movie]   = []
    
    init() {}

    init(_ page : Int, _ total_pages: Int,_ total_results : Int,_ results : [Movie]) {
        self.page           = page
        self.total_pages    = total_pages
        self.total_results  = total_results
        self.results        = results
    }
    
    init(from decoder: Decoder) throws {
        let container   = try decoder.container(keyedBy: CodingKeys.self)
        self.page           = try container.decodeIfPresent(Int.self, forKey: .page         ) ?? 0
        self.total_pages    = try container.decodeIfPresent(Int.self, forKey: .total_pages  ) ?? 0
        self.total_results  = try container.decodeIfPresent(Int.self, forKey: .total_results) ?? 0
        self.results        = try container.decodeIfPresent([Movie].self, forKey: .results  ) ?? []
    }
    
    func toData() -> Data{
        do{
            let d = try JSONEncoder().encode(self)
            return d
        }catch{
            print(error.localizedDescription)
        }
        return Data()
    }
}

struct Movie : Codable {
    var id                  : Int       = 0
    var adult               : Bool      = false
    var backdrop_path       : String    = ""
    var genre_ids           : [Int]     = []
    var original_language   : String    = ""
    var original_title      : String    = ""
    var overview            : String    = ""
    var popularity          : Float     = 0
    var poster_path         : String    = ""
    var homepage            : String    = ""
    var release_date        : String    = ""
    var title               : String    = ""
    var video               : Bool      = false
    var vote_average        : Float     = 0
    var vote_count          : Int       = 0
    var genres              : [JLGeners] = []
    var production_companies : [JLProductionCompanies] = []
    
    init() {}
   
    
    init(from decoder: Decoder) throws {
        let container   = try decoder.container(keyedBy: CodingKeys.self)
        self.adult              = try container.decodeIfPresent(Bool  .self, forKey: .adult             ) ?? false
        self.backdrop_path      = try container.decodeIfPresent(String.self, forKey: .backdrop_path     ) ?? ""
        self.genre_ids          = try container.decodeIfPresent([Int] .self, forKey: .genre_ids         ) ?? []
        self.id                 = try container.decodeIfPresent(Int   .self, forKey: .id                ) ?? 0
        self.original_language  = try container.decodeIfPresent(String.self, forKey: .original_language ) ?? ""
        self.original_title     = try container.decodeIfPresent(String.self, forKey: .original_title    ) ?? ""
        self.overview           = try container.decodeIfPresent(String.self, forKey: .overview          ) ?? ""
        self.popularity         = try container.decodeIfPresent(Float .self, forKey: .popularity        ) ?? 0
        self.poster_path        = try container.decodeIfPresent(String.self, forKey: .poster_path       ) ?? ""
        self.release_date       = try container.decodeIfPresent(String.self, forKey: .release_date      ) ?? ""
        self.title              = try container.decodeIfPresent(String.self, forKey: .title             ) ?? ""
        self.video              = try container.decodeIfPresent(Bool  .self, forKey: .video             ) ?? false
        self.vote_average       = try container.decodeIfPresent(Float .self, forKey: .vote_average      ) ?? 0
        self.vote_count         = try container.decodeIfPresent(Int   .self, forKey: .vote_count        ) ?? 0
        self.genres             = try container.decodeIfPresent([JLGeners].self, forKey: .genres) ?? []
        self.production_companies = try container.decodeIfPresent([JLProductionCompanies].self, forKey: .production_companies) ?? []
        self.homepage  = try container.decodeIfPresent(String.self, forKey: .homepage ) ?? ""


    }
    
    func toData() -> Data{
        do{
            let d = try JSONEncoder().encode(self)
            return d
        }catch{
            print(error.localizedDescription)
        }
        return Data()
    }
    
    func getGnresString()->String{
        self.genres.map({$0.name}).joined(separator: " / ")
    }
    
    func getCompaniesString()->String{
        self.production_companies.map({$0.name}).joined(separator: " / ")
    }
}

struct JLGeners : Codable {
    var id      : Int = 0
    var name    : String = ""
    
    init() {}
    init(from decoder: Decoder) throws {
        let container   = try decoder.container(keyedBy: CodingKeys.self)
        self.id         = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.name       = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
    }
}

struct JLProductionCompanies: Codable {
    var id          : Int = 0
    var logo_path   : String = ""
    var name        : String = ""
    var origin_country : String = ""
    
    init() {}
    init(from decoder: Decoder) throws {
        let container       = try decoder.container(keyedBy: CodingKeys.self)
        self.id             = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.logo_path      = try container.decodeIfPresent(String.self, forKey: .logo_path) ?? ""
        self.name           = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.origin_country = try container.decodeIfPresent(String.self, forKey: .origin_country) ?? ""
    }
}


class JLLocalInfo: Object {
    @objc dynamic var timeStamp: String = ""
    @objc dynamic var page: Int = 1
    @objc dynamic var info: Data = Data()

    convenience init(timeStamp: String) {
        self.init()
        self.timeStamp = timeStamp
    }
    
    override class func primaryKey() -> String? {
        return "timeStamp"
    }
}

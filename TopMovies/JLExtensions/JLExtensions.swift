//
//  JLExtensions.swift
//  TopMovies
//
//  Created by Juan Carlos Lugardo on 05/05/21.
//

import UIKit

/*
// MARK: - Navigation
*/

extension UINavigationController{
    
    func setClearBG(){
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.view.backgroundColor = UIColor.clear
    }
    
}

/*
// MARK: - UIView
*/
extension UIView{
    func setCorener(_ radius: CGFloat){
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
    }
}


/*
// MARK: - Generics Extensions
*/
extension UITableView{
    func registerNibCell(_ name: String,_ identifier: String){
        self.register(UINib(nibName: name, bundle: nil), forCellReuseIdentifier: identifier)
    }
}

extension Double{
    
    func toDate()-> Date{
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeZone = .current
        //let localDate = dateFormatter.string(from: date)
        return date
    }
}
extension Date{
    func isToday()-> Bool{
        Calendar.current.isDateInToday(self)
    }
    
    func getDiffHour(_ compareDate: Date)-> Int{
        let diffComponents = Calendar.current.dateComponents([.hour], from: compareDate, to: Date())
        let hours = diffComponents.hour
        print("Fecha local: \( compareDate)")
        print("con \(hours) horas de diferencia")
        return hours ?? 0
    }
}


extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        if fileIsLocal(url.lastPathComponent){
            if let img = getImageFormFile(url.lastPathComponent){
                self.image = img
                return
            }
        }
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
                _ = saveImage(image: image, url.lastPathComponent)
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}


func saveImage(image: UIImage,_ name: String) -> Bool {
    guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
        return false
    }
    guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
        return false
    }
    do {
        try data.write(to: directory.appendingPathComponent(name)!)
        return true
    } catch {
        print(error.localizedDescription)
        return false
    }
}

func getImageFormFile(_ name: String)->UIImage?{
    guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
        return nil
    }
    
    guard let fileURL = directory.appendingPathComponent(name) else { return nil }
        do {
            let imageData = try Data(contentsOf: fileURL)
            return UIImage(data: imageData)
        } catch {
            print("Error loading image : \(error)")
        }
        return nil
}

func fileIsLocal(_ name: String)->Bool{
    let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
    let url = URL(fileURLWithPath: path)
    let filePath = url.appendingPathComponent(name).path
    let fileManager = FileManager.default
    return fileManager.fileExists(atPath: filePath)
}

/*
// MARK: - Data Extensions
*/
extension Data{

    func getStruct<T: Codable>(_ type : T.Type)-> T?{
        do{
            let struture = try T.decode(data: self)//try JSONDecoder().decode(self, from: self)
            return struture
        }catch{
            print(error.localizedDescription)
        }
        return nil
    }
}
extension Decodable{
    static func decode(data: Data) throws -> Self{
        let decoder = JSONDecoder()
        return try decoder.decode(Self.self, from: data)
    }
}

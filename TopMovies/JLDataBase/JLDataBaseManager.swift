//
//  JLDataBaseManager.swift
//  TopMovies
//
//  Created by Juan Carlos Lugardo on 06/05/21.
//

import Foundation
import RealmSwift


class JLDataBaseManager{
    
    static let shared   = JLDataBaseManager()

    func getLocalObjects(_ refresh: Bool)->MoviesResponse?{
        var infoLocal : MoviesResponse?
        let localRealm = try! Realm()
        let objcs = localRealm.objects(JLLocalInfo.self)
        if let fir = objcs.first{
            if let time = Double(fir.timeStamp){
                let timestampDate = time.toDate()
                if timestampDate.getDiffHour(timestampDate) > 23 || refresh{
                    return infoLocal
                }else{
                    let mvs = fir.info.getStruct(MoviesResponse.self)
                    infoLocal = mvs ?? MoviesResponse()
                }
            }
        }
        return infoLocal
    }
   
    
    func getLocalMovie(_ idMovie: Int,_ refresh: Bool)-> Movie?{
        if let infoLocal = getLocalObjects(refresh){
            for m in infoLocal.results {
                if m.id == idMovie{
                    return m
                }
            }
            return nil
        }else{
            return nil
        }
    }

    func saveMovies(_ data: MoviesResponse){
        let timeStamp = "\((Date().timeIntervalSince1970))"
        let infoData = JLLocalInfo(timeStamp: timeStamp)
        infoData.page = 1
        infoData.info = data.toData()
        saveToRealm(infoData, JLLocalInfo.self)
    }
    
    func saveToRealm<T: Object>(_ dataSave : T,_ type : T.Type){
        do{
            let realm   = try Realm()
            try realm.write {
                realm.create(type, value: dataSave, update: .modified)
            }
        }catch{
            print(error.localizedDescription)
        }
    }
    
}

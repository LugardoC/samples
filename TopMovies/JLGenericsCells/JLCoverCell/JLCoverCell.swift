//
//  JLCoverCell.swift
//  TopMovies
//
//  Created by Juan Carlos Lugardo on 05/05/21.
//

import UIKit

class JLCoverCell: UITableViewCell {

    @IBOutlet weak var contentBF: UIView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblGenere: UILabel!
    @IBOutlet weak var lblDirector: UILabel!
    @IBOutlet weak var stkRating: UIStackView!
    @IBOutlet weak var btnPlay: UIButton!
    
    var setMovie : Movie = Movie(){
        didSet{
            infoData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        updateUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateUI(){
        contentBF.setCorener(12)
        btnPlay.setCorener(8)
        imgCover.setCorener(6)
    }
    
    func infoData(){
        lblTitle.text = setMovie.title
        lblDirector.text = setMovie.overview
        imgCover.downloaded(from: IMG_BUCKET + setMovie.poster_path)
        paintStars()
    }
    
    func paintStars(){
        let modStar = Int(setMovie.vote_average / 2)

        for i in 0..<5{
            if let imgS = self.stkRating.subviews[i] as? UIImageView{
                if modStar > i{
                    imgS.image = UIImage(named: "ic_star_fill")
                }else{
                    imgS.image = UIImage(named: "ic_star_stroke")
                }
            }
        }
    }
    
    /*
    // MARK: - Data Configuration
    */
    
    
    /*
    // MARK: - IBActions
    */
    @IBAction func showActions(_ sender: Any) {
        
    }
}
